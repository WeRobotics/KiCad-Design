# WeR components library

If a component is not in the standard KiCad libraries and not in one of the libraries included as submodule download or draw the component and add it here. Components should be added to the existing library instead of adding more libraries.

Sources to download:
- (PREFERRED) https://www.snapeda.com/
- https://www.ultralibrarian.com/
- https://octopart.com/

Please add component in the table below.

|MPN|Link|SYM|FP|STP|Date|
|---|---|---|---|---|---|
|PCA9616PW,118|https://www.snapeda.com/parts/PCA9616PW%2C118/NXP%20USA/view-part/1232864/?ref=search&t=pca9616|x|x|x|2021-10-14|
|NCP715MX50TBG|https://app.ultralibrarian.com/details/6d735842-c011-11eb-9033-0a34d6323d74/ON-Semiconductor/NCP715MX50TBG?uid=46ce8d16c13e9909|x|x|x|2021-10-14|
|TMM-105-01-T-S-SM|https://www.snapeda.com/parts/TMM-105-01-T-S-SM/Samtec%20Inc./view-part/2854566/?ref=search&t=TMM-105-01-T-S-SM||x|x|2021-10-14|
|TMM-104-01-T-S-SM|https://www.snapeda.com/parts/TMM-104-01-T-S-SM/Samtec%20Inc./view-part/2857946/?ref=search&t=%20TMM-104-01-T-S-SM%20||x|x|2021-10-14|
|||||||
