EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR02
U 1 1 60AAA83E
P 5400 2250
F 0 "#PWR02" H 5400 2000 50  0001 C CNN
F 1 "GND" H 5405 2077 50  0000 C CNN
F 2 "" H 5400 2250 50  0001 C CNN
F 3 "" H 5400 2250 50  0001 C CNN
	1    5400 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2450 2100 2450
Wire Wire Line
	3750 2150 3750 2450
Connection ~ 2100 2450
Wire Wire Line
	2100 1950 2100 2450
Wire Wire Line
	1600 1650 1600 1550
Wire Wire Line
	1600 2450 2100 2450
Wire Wire Line
	1600 1950 1600 2450
$Comp
L power:GND #PWR01
U 1 1 60A9E9A3
P 2100 2450
F 0 "#PWR01" H 2100 2200 50  0001 C CNN
F 1 "GND" H 2105 2277 50  0000 C CNN
F 2 "" H 2100 2450 50  0001 C CNN
F 3 "" H 2100 2450 50  0001 C CNN
	1    2100 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 1550 2450 1550
Connection ~ 1600 1550
$Comp
L Humidity_HIH6XX:HIH6030-021-001 U1
U 1 1 60A91AFA
P 2350 1550
F 0 "U1" H 3100 1815 50  0000 C CNN
F 1 "HIH6131-021-001" H 3100 1724 50  0000 C CNN
F 2 "Honeywell_HIH6030:Honeywell-HIH6030-021-001-MFG" H 2350 1950 50  0001 L CNN
F 3 "https://sensing.honeywell.com/index.php?ci_id=147070" H 2350 2050 50  0001 L CNN
F 4 "No" H 2350 2150 50  0001 L CNN "automotive"
F 5 "IC" H 2350 2250 50  0001 L CNN "category"
F 6 "Sensors" H 2350 2350 50  0001 L CNN "device class L1"
F 7 "Temperature and Humidity Sensors" H 2350 2450 50  0001 L CNN "device class L2"
F 8 "unset" H 2350 2550 50  0001 L CNN "device class L3"
F 9 "SENS HUMI/TEMP 3.3V I2C 4.5% SMD" H 2350 2650 50  0001 L CNN "digikey description"
F 10 "480-5704-1-ND" H 2350 2750 50  0001 L CNN "digikey part number"
F 11 "2.055mm" H 2350 2850 50  0001 L CNN "height"
F 12 "I2C" H 2350 2950 50  0001 L CNN "interface"
F 13 "Yes" H 2350 3050 50  0001 L CNN "lead free"
F 14 "2a06635c67bbf608" H 2350 3150 50  0001 L CNN "library id"
F 15 "Honeywell" H 2350 3250 50  0001 L CNN "manufacturer"
F 16 "5.5V" H 2350 3350 50  0001 L CNN "max supply voltage"
F 17 "2.3V" H 2350 3450 50  0001 L CNN "min supply voltage"
F 18 "Board Mount Humidity Sensors I2C 4.5%RH SOIC-8SMD non-condensing" H 2350 3550 50  0001 L CNN "mouser description"
F 19 "785-HIH-6030-021-001" H 2350 3650 50  0001 L CNN "mouser part number"
F 20 "0.65mA" H 2350 3750 50  0001 L CNN "operating supply current"
F 21 "SOIC8" H 2350 3850 50  0001 L CNN "package"
F 22 "14bits" H 2350 3950 50  0001 L CNN "resolution"
F 23 "Yes" H 2350 4050 50  0001 L CNN "rohs"
F 24 "I2C" H 2350 4150 50  0001 L CNN "sensor output"
F 25 "0.15mm" H 2350 4250 50  0001 L CNN "standoff height"
F 26 "+100°C" H 2350 4350 50  0001 L CNN "temperature range high"
F 27 "-40°C" H 2350 4450 50  0001 L CNN "temperature range low"
	1    2350 1550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 60A6E82B
P 2100 1800
F 0 "C2" H 2215 1846 50  0000 L CNN
F 1 "100uF" H 2215 1755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 2138 1650 50  0001 C CNN
F 3 "~" H 2100 1800 50  0001 C CNN
	1    2100 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 60A6E259
P 1600 1800
F 0 "C1" H 1715 1846 50  0000 L CNN
F 1 "0.2uF" H 1715 1755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1638 1650 50  0001 C CNN
F 3 "~" H 1600 1800 50  0001 C CNN
	1    1600 1800
	1    0    0    -1  
$EndComp
$Comp
L Header_Connectors:BM04B-GHS-TBT_LFSNN J1
U 1 1 60ABFD74
P 5800 1450
F 0 "J1" H 6328 1353 60  0000 L CNN
F 1 "BM04B-GHS-TBT_LFSNN" H 6328 1247 60  0000 L CNN
F 2 "Connector_BM04:BM04B-GHS-TBT LFSNN" H 6200 1190 60  0001 C CNN
F 3 "" H 5800 1450 60  0000 C CNN
	1    5800 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2100 1650 2450 1650
Wire Wire Line
	5400 2250 5400 1750
Wire Wire Line
	5150 1450 5800 1450
Wire Wire Line
	5150 1550 5800 1550
Wire Wire Line
	5150 1650 5800 1650
Wire Wire Line
	5400 1750 5800 1750
Wire Wire Line
	1400 1550 1600 1550
Text Label 5150 1450 0    50   ~ 0
5V
Text Label 1400 1550 0    50   ~ 0
5V
Text Label 5150 1550 0    50   ~ 0
I2C_SCL
Text Label 4650 1550 0    50   ~ 0
I2C_SCL
Text Label 5150 1650 0    50   ~ 0
I2C_SDA
Text Label 4650 1650 0    50   ~ 0
I2C_SDA
Wire Wire Line
	3750 1650 4650 1650
Wire Wire Line
	3750 1550 4650 1550
$EndSCHEMATC
